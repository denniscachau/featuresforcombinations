<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{featuresforcombinations}prestashop>featuresforcombinations_8b670f8847e6dd1ff679b8073d237323'] = 'Características para las combinaciones';
$_MODULE['<{featuresforcombinations}prestashop>featuresforcombinations_bb8956c67b82c7444a80c6b2433dd8b4'] = '¿Está seguro de que desea desinstalar este módulo?';
$_MODULE['<{featuresforcombinations}prestashop>displayadminproductsextra_98f770b0af18ca763421bac22b4b6805'] = 'Características';
$_MODULE['<{featuresforcombinations}prestashop>displayadminproductsextra_8408c9e822e4412516d0ac876d683b6c'] = 'Añadir característica';
$_MODULE['<{featuresforcombinations}prestashop>product_feature171_21021ea0e52be8e9c599f4dff41e5be0'] = 'Característica';
$_MODULE['<{featuresforcombinations}prestashop>product_feature171_5ebd6e1c1cdeac7a7c1896ab842d98da'] = 'Selecciona una característica';
$_MODULE['<{featuresforcombinations}prestashop>product_feature171_ce0048fd3dcb32c2082a41180e1a2aef'] = 'Valor predefinido';
$_MODULE['<{featuresforcombinations}prestashop>product_feature171_fd380496c889dcb05d6e996fd1ecaf0b'] = 'Selecciona un valor';
$_MODULE['<{featuresforcombinations}prestashop>product_feature171_30b916a82aaec374bd99a832e1139693'] = 'O un valor personalizado';
$_MODULE['<{featuresforcombinations}prestashop>displayadminproductscombinationbottom_98f770b0af18ca763421bac22b4b6805'] = 'Características';
$_MODULE['<{featuresforcombinations}prestashop>displayadminproductscombinationbottom_8408c9e822e4412516d0ac876d683b6c'] = 'Añadir característica';
$_MODULE['<{featuresforcombinations}prestashop>product_feature_21021ea0e52be8e9c599f4dff41e5be0'] = 'Característica';
$_MODULE['<{featuresforcombinations}prestashop>product_feature_5ebd6e1c1cdeac7a7c1896ab842d98da'] = 'Selecciona una característica';
$_MODULE['<{featuresforcombinations}prestashop>product_feature_ce0048fd3dcb32c2082a41180e1a2aef'] = 'Valor predefinido';
$_MODULE['<{featuresforcombinations}prestashop>product_feature_fd380496c889dcb05d6e996fd1ecaf0b'] = 'Selecciona un valor';
$_MODULE['<{featuresforcombinations}prestashop>product_feature_30b916a82aaec374bd99a832e1139693'] = 'O un valor personalizado';
