<?php
/**
 * 2007-2021 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2021 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class FFC extends Product
{
    public static function getAllForProductAttribute($id_product, $id_product_attribute)
    {
        if (!Feature::isFeatureActive()) {
            return array();
        }
        $context = Context::getContext();

        $query = "SELECT ffc.`id_feature`, ffc.`id_feature_value`, fv.`custom`
                FROM `" . _DB_PREFIX_ . "featuresforcombinations` ffc
                LEFT JOIN `" . _DB_PREFIX_ . "feature_value` fv
                    ON (ffc.`id_feature_value` = fv.`id_feature_value`)
                WHERE `id_product` = " . (int)$id_product . "
                    AND `id_product_attribute` = " . (int)$id_product_attribute;

        $result = Db::getInstance()->executeS($query);

        $custom_values = array();
        foreach ($result as &$res) {
            $res['predefined_values'] = FeatureValue::getFeatureValuesWithLang(
                $context->language->id,
                $res['id_feature']
            );
            if ($res['custom']) {
                $customs = Db::getInstance()->executeS(
                    "SELECT `value`, `id_lang`
                    FROM `" . _DB_PREFIX_ . "feature_value_lang`
                    WHERE `id_feature_value` = " . (int)$res['id_feature_value']
                );

                if (!empty($customs)) {
                    $custom_values[(int)$res['id_feature_value']] = array();
                    foreach ($customs as $cus) {
                        $custom_values[(int)$res['id_feature_value']][(int)$cus['id_lang']] = $cus['value'];
                    }
                }
            }
        }

        return array('features' => $result, 'custom_values' => $custom_values);
    }

    public function addCombinationsFeaturesToDB($id_product_attribute, $id_feature, $id_value, $custom = false)
    {
        if ($custom) {
            $row = array('id_feature' => (int)$id_feature, 'custom' => 1);
            Db::getInstance()->insert('feature_value', $row);
            $id_value = Db::getInstance()->Insert_ID();
        }
        $row = array(
            'id_product' => (int)$this->id,
            'id_product_attribute' => (int)$id_product_attribute,
            'id_feature' => (int)$id_feature,
            'id_feature_value' => (int)$id_value
        );
        Db::getInstance()->insert('featuresforcombinations', $row);
        SpecificPriceRule::applyAllRules(array((int)$this->id));
        if ($id_value) {
            return $id_value;
        }
    }

    public function deleteCombinationsFeatures($id_product_attribute)
    {
    // @see AdminProductsController::deleteFeatures
        $features = Db::getInstance()->executeS(
            "SELECT ffc.*, fv.*
            FROM `" . _DB_PREFIX_ . "featuresforcombinations` as ffc
            LEFT JOIN `" . _DB_PREFIX_ . "feature_value` as fv ON (fv.`id_feature_value` = ffc.`id_feature_value`)
            WHERE ffc.`id_product` = " . (int) $this->id . "
                AND ffc.`id_product_attribute` = " . (int)$id_product_attribute
        );

        foreach ($features as $tab) {
            if ($tab['custom']) {
                Db::getInstance()->delete(
                    'feature_value',
                    '`id_feature_value` = ' . (int)$tab['id_feature_value']
                );
                Db::getInstance()->delete(
                    'feature_value_lang',
                    '`id_feature_value` = ' . (int)$tab['id_feature_value']
                );
            }
        }

        Db::getInstance()->delete(
            'featuresforcombinations',
            '`id_product` = ' . (int)$this->id . ' AND `id_product_attribute` = ' . (int)$id_product_attribute
        );
    }

    public static function getCombinationsFeatures($id_product, $id_product_attribute, $id_lang = null)
    {
        if ($id_lang == null) {
            $id_lang = (int)Context::getContext()->language->id;
        }

        $query = "SELECT DISTINCT fl.`name`, fvl.`value`
            FROM `" . _DB_PREFIX_ . "featuresforcombinations` ffc
            CROSS JOIN `" . _DB_PREFIX_ . "feature_product` pf
            LEFT JOIN `" . _DB_PREFIX_ . "feature_lang` fl
                ON (ffc.`id_feature` = fl.`id_feature`
                    AND fl.`id_lang` = " . (int)$id_lang . ")
            LEFT JOIN `" . _DB_PREFIX_ . "feature_value_lang` fvl
                ON (ffc.`id_feature_value` = fvl.`id_feature_value`
                    AND fvl.`id_lang` = " . (int)$id_lang . ")
            INNER JOIN `" . _DB_PREFIX_ . "feature` f
                ON (f.`id_feature` = pf.`id_feature`
                    OR  f.`id_feature` = ffc.`id_feature`)" . Shop::addSqlAssociation('feature', 'f') . "
            WHERE ffc.`id_product` = " . (int)$id_product . "
                AND ffc.`id_product_attribute` = " . (int)$id_product_attribute . "
                AND pf.`id_product` = " . (int)$id_product . "
            ORDER BY f.`position`";

        return Db::getInstance()->executeS($query);
    }
}
